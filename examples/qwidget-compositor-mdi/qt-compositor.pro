TEMPLATE = app
TARGET = qwidget-compositor-mdi
DEPENDPATH += .
INCLUDEPATH += .

# comment out the following to not use pkg-config in the pri files
CONFIG += use_pkgconfig

DESTDIR=$$PWD/../../bin/

include (../../src/qt-compositor/qt-compositor.pri)

QT += widgets gui-private widgets-private

# Input
SOURCES += main.cpp

CONFIG += qt warn_on debug  create_prl link_prl
OBJECTS_DIR = .obj/release-shared
MOC_DIR = .moc/release-shared
