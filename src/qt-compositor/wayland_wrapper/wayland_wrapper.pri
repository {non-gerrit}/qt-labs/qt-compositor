HEADERS += \
    $$PWD/wlcompositor.h \
    $$PWD/wldisplay.h \
    $$PWD/wloutput.h \
    $$PWD/wlshmbuffer.h \
    $$PWD/wlsurface.h \
    $$PWD/wlselection.h \
    $$PWD/wldrag.h

SOURCES += \
    $$PWD/wlcompositor.cpp \
    $$PWD/wldisplay.cpp \
    $$PWD/wloutput.cpp \
    $$PWD/wlshmbuffer.cpp \
    $$PWD/wlsurface.cpp \
    $$PWD/wlselection.cpp \
    $$PWD/wldrag.cpp
