TEMPLATE = lib

include (qt-compositor.pri)

installPath = $$INSTALLBASE

target.path = $$installPath/lib
headers_path = $$installPath/include

headers.path = $$headers_path/qt-compositor
headers.files = $$HEADERS

INSTALLS = target headers

QT += gui-private
